import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Flighttype } from './flighttype';
import { PageService } from 'src/app/share/page.service';

@Component({
  selector: 'app-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.css']
})
export class FlightComponent implements OnInit {

  startDate = new Date(Date.now())
  formflight !: FormGroup
  arrflight !: Flighttype[]

  constructor(private flight_form: FormBuilder , public page: PageService) {
    this.formflight = this.flight_form.group({
      fullname: ['', Validators.required],
      from: [null, Validators.required],
      to: [null, Validators.required],
      type: ['', Validators.required],
      adults: [0,[Validators.required , Validators.max(10), Validators.pattern('[0-9]*$')]],
      departure: ['', Validators.required],
      children: [0,[Validators.required , Validators.max(10), Validators.pattern('[0-9]*$')]],
      infants: [0,[Validators.required , Validators.max(10), Validators.pattern('[0-9]*$')]],
      arrival: ['', Validators.required],

    })

  }

  ngOnInit(): void {
    this.getPage()
  }

  getPage(){
    this.arrflight = this.page.getPage()
  }
  onSubmit(f:Flighttype): void{
    const yearDeparture = f.departure.getFullYear() + 543
    const yearArrival = f.arrival.getFullYear() + 543
    f.departure = new Date((f.departure.getMonth() + 1) + '/' + f.departure.getDate() + "/" + yearDeparture)
    f.arrival = new Date((f.arrival.getMonth() + 1) + '/' + f.arrival.getDate() + "/" + yearArrival)
    this.page.addflight(f)

  }
}

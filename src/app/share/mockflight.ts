import { Flighttype } from "../component/flight/flighttype";



export class Mockflight {

  public static mock_flight: Flighttype[] = [
    {
      fullname: "Tony katak",
      from: "ไทย",
      to: "ญี่ปุ่น",
      type: "one way",
      adults: 2,
      departure: new Date('04-03-2565'),
      children: 0,
      infants: 0,
      arrival: new Date('12-12-2565')
    }

  ]


}

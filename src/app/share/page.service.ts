import { Injectable } from '@angular/core';
import { Mockflight } from './mockflight';
import { Flighttype } from '../component/flight/flighttype';


@Injectable({
  providedIn: 'root'
})
export class PageService {

  flight: Flighttype[] = []

  constructor() {
    this.flight = Mockflight.mock_flight


   }
   getPage(){
     return this.flight
   }
   addflight(f:Flighttype){
    this.flight.push(f)
   }
}
